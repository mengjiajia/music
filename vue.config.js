const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
    transpileDependencies: true,
    publicPath: process.env.NODE_ENV == 'production' ? '/' : '/',
    lintOnSave: process.env.NODE_ENV == 'development',
    devServer: {
        open: true,
        port: 8282,
        hot: true,
    },
})
