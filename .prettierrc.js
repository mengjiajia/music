// https://prettier.io/docs/en/options.html
module.exports = {
    // 一行的字符数，如果超过会进行换行，默认为80
    printWidth: 100,

    // 一个tab代表几个空格数，默认就是2
    tabWidth: 4,

    // 启用tab取代空格符缩进，默认为false
    useTabs: false,

    // 行尾是否使用分号，默认为true
    semi: false,

    // 字符串是否使用单引号
    singleQuote: true,

    // 给对象里的属性名是否要加上引号
    quoteProps: 'as-needed',

    // 在jsx里是否使用单引号
    jsxSingleQuote: false,

    // 是否使用尾逗号，可选值"<none|es5|all>"
    trailingComma: 'es5',

    // 对象大括号直接是否有空格
    bracketSpacing: true,

    //JSX标签闭合位置 默认false
    jsxBracketSameLine: false,

    // 总是有括号 可选 avoid|always  avoid 能省略就省略
    arrowParens: 'avoid',

    // 只格式化文件的一个段
    rangeStart: 0,

    // 指定要使用的解析器。
    parser: '',

    // 指定要使用的文件名，以推断要使用哪个解析器
    filepath: '',

    // Prettier可以将自己限制为只格式化文件顶部包含特殊注释 @prettier 的文件
    requirePragma: false,

    // 是否可以在文件的顶部插入一个特殊的 @format 标记，表示该文件已被Prettier格式化
    insertPragma: false,

    proseWrap: 'always',

    //html空格严格程度 可选 css|strict|ignore
    htmlWhitespaceSensitivity: 'strict',

    // 是否缩进Vue文件中<script>和<style>标记内的代码
    vueIndentScriptAndStyle: true,

    endOfLine: 'auto',

    embeddedLanguageFormatting: 'auto',
}
