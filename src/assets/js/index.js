export const numberFormat = val => {
    let num = 10000
    var sizesValue = ''
    /**
     * 判断取哪个单位
     */
    if (val < 1000) {
        // 如果小于1000则直接返回
        sizesValue = ''
    } else if (val > 1000 && val < 9999) {
        sizesValue = '千'
    } else if (val > 10000 && val < 99999999) {
        sizesValue = '万'
    } else if (val > 100000000) {
        sizesValue = '亿'
    }
    /**
     * 大于一万则运行下方计算
     */
    let i = Math.floor(Math.log(val) / Math.log(num))
    /**
     * toFixed(0)看你们后面想要取值多少，我是不取所以填了0，一般都是取2个值
     */
    var sizes = (val / Math.pow(num, i)).toFixed(0)
    sizes = sizes + sizesValue
    return sizes
}
