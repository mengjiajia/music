const VUE_APP_MODE = process.env.VUE_APP_MODE;
let baseURL;
if (VUE_APP_MODE == 'development') {
    // 本地
    baseURL = 'http://music.mengjia.site'; //测试环境
} else {
    //pre 预生产
    // prod 生产
    switch (VUE_APP_MODE) {
        case 'test': //测试
            baseURL = 'http://music.mengjia.site';
            break;
        case 'pre': // 预生产
            baseURL = '//resource.apipre.securityuni.cn';
            break;
        case 'prod': // 生产
            baseURL = '//resource.api.securityuni.cn';
            break;
        default: //测试
            baseURL = 'http://music.mengjia.site';
            break;
    }
}

module.exports = {
    baseURL,
};
