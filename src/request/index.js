/**
 * 请求封装
 */
import axios from 'axios'
import configUrl from './url.js'
const service = axios.create({
    baseURL: configUrl.baseURL,
    // withCredentials: true, // 当跨域请求时发送cookie
    // timeout: 15000 // 请求超时
})

service.interceptors.request.use(
    config => {
        if (config.url.indexOf('?') > -1) {
            config.url += '&timeStamp=' + new Date().getTime()
        } else {
            config.url += '?timeStamp=' + new Date().getTime()
        }
        return config
    },
    error => {
        console.log(error)
        return Promise.reject(error)
    }
)

service.interceptors.response.use(
    response => {
        if (response.status == 200) {
            return response.data
        }
    },
    error => {
        return Promise.reject(error)
    }
)

export default service
