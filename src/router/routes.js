export default [
    {
        path: '/',
        name: 'home',
        component: () => import(/* webpackChunkName: "home" */ '@/view/home'),
        meta: { title: '首页', name: 'home' },
    },
    {
        path: '/searchPage',
        name: 'searchPage',
        component: () => import(/* webpackChunkName: "searchPage" */ '@/view/searchPage'),
        meta: { title: '搜索', name: 'searchPage' },
    },
]
