import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';

Vue.use(VueRouter);

const createRouter = () =>
    new VueRouter({
        mode: 'history',
        base: process.env.NODE_ENV == 'production' ? '/' : '/',
        scrollBehavior() {},
        routes,
    });

const router = createRouter();

export default router;
