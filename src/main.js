import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import vant from 'vant'
import 'vant/lib/index.css'
import 'element-ui/lib/theme-chalk/index.css'
import '@/assets/style/public.sass'
Vue.config.productionTip = false

Vue.use(ElementUI).use(vant)
new Vue({
    store,
    router,
    render: h => h(App),
}).$mount('#app')
