import request from '@/request'

// 获取默认搜索关键词
export const defaultSearchKeys = (data = '') => {
    return request({
        url: '/search/default',
        method: 'post',
        data,
    })
}
// 获取热门搜索列表
export const hotSearchDetail = (data = '') => {
    return request({
        url: '/search/hot/detail',
        method: 'post',
        data,
    })
}
// 搜索
export const cloudsearch = (data = '') => {
    let url = '/cloudsearch'
    let isParams = Object.keys(data)
    if (isParams.length > 0) {
        let queryArr = []
        url += '?'
        isParams.forEach(item => {
            queryArr.push(item + '=' + data[item])
        })
        url += queryArr.join('&')
    }
    return request({
        url,
        method: 'post',
    })
}
