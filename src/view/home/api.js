import request from '@/request'

// 获取首页发现数据
export const getindexData = (data = '') => {
    return request({
        url: '/homepage/block/page',
        method: 'post',
        data,
    })
}

// 获取首页推荐歌单
export const getSongSheet = (data = '') => {
    return request({
        url: '/personalized',
        method: 'post',
        data,
    })
}
