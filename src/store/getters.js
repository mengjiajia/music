export default {
    loginStatus: state => state.user.loginStatus,
    audioShow: state => state.audio.audioShow,
    audioList: state => state.audio.audioList,
}
