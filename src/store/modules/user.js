const getLoginStatus = () => {
    return 333;
};

const getDefaultState = () => {
    return {
        loginStatus: getLoginStatus(),
    };
};

const state = getDefaultState();
const mutations = {};
const actions = {};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
};
