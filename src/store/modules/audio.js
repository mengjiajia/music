const getAudioShow = () => {
    let audioShow = sessionStorage.getItem('audioShow')
    audioShow = audioShow ? true : false
    return audioShow
}
const getAudioList = () => {
    let audioList = sessionStorage.getItem('audioList')
    audioList = audioList || ''
    return audioList
}

const getDefaultState = () => {
    return {
        audioShow: getAudioShow(),
        audioList: getAudioList(),
    }
}

const state = getDefaultState()
const mutations = {
    SET_AUDIOSHOW: (state, audioShow) => {
        if (audioShow) {
            sessionStorage.setItem('audioShow', audioShow)
            state.audioShow = audioShow
        } else {
            sessionStorage.removeItem('audioShow')
            state.audioShow = ''
        }
    },
    SET_AUDIOLIST: (state, audioList) => {
        if (audioList) {
            sessionStorage.setItem('audioList', audioList)
            state.audioList = audioList
        } else {
            sessionStorage.removeItem('audioList')
            state.audioList = ''
        }
    },
}
const actions = {}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
}
