import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import user from './modules/user'
import audio from './modules/audio'
Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        user,
        audio,
    },
    getters,
})

export default store
