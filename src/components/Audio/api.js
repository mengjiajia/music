import request from '@/request'

export const getLoginStatus = (data = '') => {
    return request({
        url: '/login/status',
        method: 'post',
        data,
    })
}

// 获取热门推荐歌单分类
export const getHotTuijian = (data = '') => {
    return request({
        url: '/playlist/hot',
        method: 'post',
        data,
    })
}
// 获取热门推荐歌单
// export const getHotTuijianPlaylist = (data = '') => {
//   return request({
//     url: '/personalized',
//     method: 'post',
//     data,
//   })
// }
export const getHotTuijianPlaylist = (data = '') => {
    return request({
        url: '/top/playlist/highquality',
        method: 'post',
        data,
    })
}

// 获取歌曲详情
export const getSongInfo = (data = {}) => {
    return request({
        url: '/song/detail',
        method: 'post',
        data,
    })
}

// 获取歌曲播放地址
export const getSongUrls = (data = {}) => {
    let url = '/song/url/v1'
    let isParams = Object.keys(data)
    if (isParams.length > 0) {
        let queryArr = []
        url += '?'
        isParams.forEach(item => {
            queryArr.push(item + '=' + data[item])
        })
        url += queryArr.join('&')
    }
    return request({
        url,
        method: 'post',
    })
}
// 获取歌曲播放地址
export const getSongLyric = (data = {}) => {
    let url = '/lyric'
    let isParams = Object.keys(data)
    if (isParams.length > 0) {
        let queryArr = []
        url += '?'
        isParams.forEach(item => {
            queryArr.push(item + '=' + data[item])
        })
        url += queryArr.join('&')
    }
    return request({
        url,
        method: 'post',
    })
}
